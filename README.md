# Powershell Scripts

## About this project

This repository contains various short Powershell scripts, which I would like to keep in a uniform central location so that all users can always obtain the latest version. There's nothing earth-shattering about it, but it's sure to come at some point.

## Description

List of scripts:

| Filename | Description |
| ---- | ----------- |
| [countdown.ps1](countdown.ps1) | Displays a countdown and shows the remaining seconds. |
| [messagebox.ps1](messagebox.ps1) | Displays a message in a dialog window. |
| [waitandrun.ps1](waitandrun.ps1) | Waits until the network is reachable and runs another script. |
| [waitforserver.ps1](waitforserver.ps1) | Waits until a connection to a host is online. |

## Installation

Simply clone the repository somewhere into your file system:

```
git clone https://gitlab.com/Munahid/powershell-scripts.git
```

## License

The files in this repository may be used under the terms of the [MIT License](LICENSE). I hope you find one or the other script useful. Thank you for using this software.

Please open a [ticket](https://gitlab.com/Munahid/powershell-scripts/-/issues) if you have a good idea for possible improvements or not yet existing but much needed powershell scripts.
