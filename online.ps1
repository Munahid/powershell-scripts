# powershell -noprofile -executionpolicy bypass -file P:\check.ps1

$servers = "fritz.box", "proxmox1", "nas", "nas2", "mail", "www.gasi.de", "druckerlexmarkcx510.fritz.box", "192.168.178.26", "tvsamsung1.fritz.box", "www.google.de", "mg.mud.de"

While ($True) {
  $results = @{}

  Foreach ($server in $servers) {
    $results[$server] = Test-Connection -BufferSize 32 -Count 1 -ComputerName $server -Quiet
  }

  Clear

  Foreach ($server in $servers) {
    $result = $results[$server];
    Write-Host -NoNewline $server -ForegroundColor Blue
    Write-Host -NoNewline ": "

    If ($result) {
      Write-Host -ForegroundColor Yellow "online"
    } Else {
      Write-Host -ForegroundColor Red "offline"
    }
  }

  Start-Sleep -Seconds 1
}
