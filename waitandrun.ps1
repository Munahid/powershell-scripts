# Der Skript wartet, bis das Netzwerk da ist und startet dann einen
# anderen Skript im Netzwerk. Das verhindert Startprobleme von Skripten,
# die im Autostart liegen, weil das Netzwerk das lokalen APC erst
# eine Sekunde später erreichbar ist.
#
# Statt etwa:
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile
#   -executionpolicy bypass -file \\fs\test.ps1 123
# starten Sie:
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile
#   -executionpolicy bypass -file c:\waitandrun.ps1 \\fs\test.ps1 123
#
# Der Skript wartet auf den spezifizierten Server, startet den im ersten
# Parameter übergebenen Skript und übergibt diesem die weiteren Parameter.

Param(
  [string]$command = '',
  [string]$parameters = ''
)


if ($command.Length -lt 1) {
  Write-Host "Es wurde kein auszuführender zweiter Skript angegeben."
  Read-Host -Prompt "Return drücken zum Beenden"
  Break
}

$servername = Select-String -InputObject "$command" -pattern "(?<=\\\\)(.*?)(?=(\\))" | Select -ExpandProperty Matches | Select -ExpandProperty Value
#echo $servername

. $PSScriptRoot\waitforserver.ps1 $servername

#echo $PsBoundParameters.Values
#echo "---"
#echo $args   # unbound parameters

. $command $parameters $args
