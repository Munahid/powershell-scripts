# Der Skript bekommt einen Servernamen übergeben  und wartet, bis
# dieser über das Netzwerk erreichbar ist.
#
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile
#   -executionpolicy bypass -file waitforserver.ps1 xyz

Param(
  [string]$servername = ''
)

if ($command.Length -lt 1) {
  Write-Host "Es wurde kein Servername angegeben."
  Read-Host -Prompt "Return drücken zum Beenden"
  Break
}

# Es geht nur weiter, wenn der Server online ist.
While ((Test-Connection -Quiet -Verbose -ComputerName "$servername" -Count 1) -eq $False) {
  Write-Host "Kann Server $servername nicht erreichen. Warte 1s."; Sleep -Seconds 1
}
