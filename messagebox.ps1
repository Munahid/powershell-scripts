# Displays a message in a dialog window.
#
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile
#   -executionpolicy bypass -file messagebox.ps1 message title

Param(
  [string]$message = '',
  [string]$title = ''
)

[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[System.Windows.Forms.MessageBox]::Show($message, $title, 0, [System.Windows.Forms.MessageBoxIcon]::Information)
