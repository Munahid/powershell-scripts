# Der Skript bekommt eine Zeitspanne in Sekunden eine Mitteilung übergeben
# und zeigt die Mitteilung mit Hilfe der Write-Progress-Funktion an.
# Der Coundown ist per Tastendruck abbrechbar. Leider befindet sich die
# Progress-Bar an einer festen Position (da PS ggf. mehrere gleichzeitig
# anzeigt).
#
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile
#   -executionpolicy bypass -file countdown.ps1 10 "Bitte warten."

Param(
  [int]$waitTime = 5,
  [string]$message = "Die Pause kann mit Tastendruck beendet werden."
)

# Progress-Bar ausgeben.

$waitTimePerLoop = $waitTime/100
For ($i=0; $i -le 100; $i++) {    # Wait-Progress counts until 100 percent
  Start-Sleep -Milliseconds ($waitTimePerLoop*1000)
  $statustext = "Es wird noch {0} Sekunden gewartet." -f ($waitTime-[int]($i * $waitTimePerLoop))
  Write-Progress -Activity " " -Status $statustext -PercentComplete $i -CurrentOperation "$message"
  if ([Console]::KeyAvailable) {
    $pressedKey = [Console]::ReadKey($true)
    $i = 100  # finished
    Write-Progress -Activity " " -PercentComplete 100 -Completed
  }
}
